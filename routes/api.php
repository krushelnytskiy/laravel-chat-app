<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('search', 'Api\Search@search');
Route::post('chat/create', ['uses' => 'Api\Chat@create', 'middleware' => ['auth:api']] );
Route::post('chat/download', ['uses' => 'Api\Chat@download', 'middleware' => ['auth:api']] );
Route::post('chat/send', ['uses' => 'Api\Chat@send', 'middleware' => ['auth:api']] );
Route::post('chat/list', ['uses' => 'Api\Chat@list', 'middleware' => ['auth:api']] );