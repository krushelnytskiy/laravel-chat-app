<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'Index@home', 'middleware' => ['auth:web']]);

Route::get('login', 'User@showLoginForm')->name('login');
Route::post('login', 'User@login');
Route::any('logout', 'User@logout')->name('logout');
Route::get('register', 'User@showRegistrationForm')->name('register');
Route::post('register', 'User@register');
Route::get('password/reset', 'Password@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Password@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Password@showResetForm')->name('password.reset');
Route::post('password/reset', 'Password@reset');
