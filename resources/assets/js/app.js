/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Vue from 'vue'
Vue.use(require('vue-resource'));

/**
 * @type {Vue}
 */
const app = new Vue({
    self: this,
    el: '#app',
    methods: {
        // Search users through API
        search: function () {
            this.error = false;
            this.resultUsers = [];

            this.$http.get('/api/search?keyword=' + this.keyword).then(function(response) {
                response.data.error ? (this.error = response.data.error) : this.resultUsers = response.data;
                this.keyword = '';
                this.chat = false;
            });
        },
        // Get list of chats, that was predefined
        list: function () {
            this.chats = [];
            this.error = false;

            this.$http.post('/api/chat/list', {api_token: window.Laravel.apiToken}).then(
                function (response) {
                    if (response.data.hasOwnProperty('list')) {
                        this.chats = response.data.list;
                    } else if (response.data.hasOwnProperty('error')) {
                        this.error = response.data.error;
                    }
                }
            )
        },
        // Store chat when user choosed
        storeChat: function (userId) {
            this.$http.post('/api/chat/create', {id: userId, api_token: window.Laravel.apiToken}).then(
                function (response) {
                    if (response.data.hasOwnProperty('chat')) {
                        this.download(response.data.chat);
                        this.list();
                        this.resultUsers = [];
                    }
                }
            );
        },
        // Download message list from chat
        download: function (chat) {
            this.error = '';
            this.messages = [];
            this.chat = false;

            this.$http.post('/api/chat/download', {chat: chat, api_token: window.Laravel.apiToken}).then(function (response) {
                if (!response.data.error) {
                    this.messages = response.data;
                    this.chat = chat;
                }
            });
        },
        // Send message to backend
        send: function () {

            this.loading = true;
            this.$http.post(
                '/api/chat/send',
                {
                    chat: this.chat,
                    api_token: window.Laravel.apiToken,
                    text: this.$el.querySelector('#message_box').value
                }
            ).then(
                function (response) {
                    this.loading = false;
                    this.messages.push(response.data);
                    this.$el.querySelector('#message_box').value = '';
                },
                function () {
                    this.loading = false;
                }
            )
        }
    },
    data: {
        resultUsers: [], // Users in search result list
        error: false,    // Error to display
        keyword: '',     // Search keyword
        messages: [],    // Messages in box
        chat: false,     // Chat ID
        loading: false,  // Sending status
        chats: []        // chats
    },
    created() {
        window.Echo.join(`chat`)
            .listen('MessageSent', (response) => {
                if (response.hasOwnProperty('chat')) {
                    if (response.chat.id === this.chat) {
                        this.download(response.chat.id);
                    }

                    this.list();
                }
            });

        this.list();
    }
});