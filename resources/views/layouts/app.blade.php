<html>
<head>
    <title>App Name - @yield('title')</title>
    <script>
        window.Laravel = {
            csrfToken: '{{ csrf_token() }}'
            @if(\Illuminate\Support\Facades\Auth::check())
                , apiToken: '{{ \Illuminate\Support\Facades\Auth::user()->api_token }}'
            @endif
        };
    </script>
    <link rel="stylesheet" href="{{ URL::to('/css/app.css')  }}">
</head>
<body>
@include('partial.menu.default')
<div class="container" id="app">
    @yield('content')
</div>
<script src="{{ URL::to('/js/app.js') }}"></script>
</body>
</html>