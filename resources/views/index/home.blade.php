@extends('layouts.app')
@section('content')
    <div class="main_section">
        <div class="container">
            <div class="chat_container">
                <div class="col-sm-3 chat_sidebar">
                    <div class="row">
                        <div id="custom-search-input">
                            <div class="input-group col-md-12">
                                <input type="text" class="  search-query form-control" placeholder="Users" v-model="keyword" />
                                <button class="btn btn-danger" type="button" @click="search()">
                                <span class=" glyphicon glyphicon-search"></span>
                                </button>
                            </div>
                        </div>
                        <div class="member_list">
                            <div class="alert alert-danger" v-if="error">
                                @{{ error  }}
                            </div>
                            <ul class="list-unstyled" v-for="user in resultUsers" v-if="resultUsers">
                                <li class="left clearfix" @click="storeChat(user.id)">
                                <span class="chat-img pull-left"></span>
                                <div class="chat-body clearfix">
                                    <div class="header_sec">
                                        <strong class="primary-font">@{{ user.name  }}</strong>
                                    </div>
                                </div>
                                </li>

                            </ul>
                            <ul class="list-unstyled" v-for="chat in chats">
                                <li class="left clearfix" @click="download(chat.id)">
                                <span class="chat-img pull-left"></span>
                                <div class="chat-body clearfix">
                                    <div class="header_sec">
                                        <strong class="primary-font">@{{ chat.opposite.name }}</strong> <strong class="pull-right">
                                            @{{ chat.messages.length ? chat.messages.created_at : '' }}</strong>
                                        <span class="badge pull-right">@{{ chat.messages.filter(function(message) {return !message.read}).length }}</span>
                                    </div>
                                </div>
                                </li>
                            </ul>
                        </div></div>
                </div>
                <div class="col-sm-9 message_section">
                    <div class="row">
                        <div class="new_message_head">
                            <div class="pull-left"><button><i class="fa fa-plus-square-o" aria-hidden="true"></i> New Message</button></div><div class="pull-right"><div class="dropdown">
                                </div></div>
                        </div>
                        <div class="chat_area">
                            <ul class="list-unstyled" v-for="message in messages">
                                <li class="left clearfix">
                     <span class="chat-img1 pull-left">
                         @{{ message.creator.name }}
                     </span>
                                    <div class="chat-body1 clearfix">
                                        <p>
                                            @{{ message.text }}
                                        </p>
                                        <small v-if="!message.read" class="text-muted">Unread</small>
                                        <div class="chat_time pull-right">@{{ message.created_at }}</div>
                                    </div>
                                </li>
                            </ul>
                        </div><!--chat_area-->
                        <div class="message_write">
                            <textarea class="form-control" placeholder="type a message" id="message_box"></textarea>
                            <div class="clearfix"></div>
                            <div class="chat_bottom">
                                <button class="pull-right btn btn-success" @click="send()">Send</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection