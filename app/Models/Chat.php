<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Chat
 * @package App\Models
 * @property Collection $messages
 */
class Chat extends Model
{

    /**
     * Chat statuses
     */
    const STATUS_ACTIVE  = 'active';
    const STATUS_BLOCKED = 'blocked';
    const STATUS_DELETED = 'deleted';

    /**
     * @var string
     */
    protected $table = 'chats';

    /**
     * @var array
     */
    protected $fillable = ['creator_id', 'partner_id', 'status', 'created_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partner()
    {
        return $this->belongsTo(User::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    /**
     * @param $user
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|mixed
     */
    public function oppositeUser($user)
    {
        return $user->id === $this->creator->id ? $this->partner : $this->creator;
    }

}