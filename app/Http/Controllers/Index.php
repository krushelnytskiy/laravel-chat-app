<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

/**
 * Class IndexController
 * @package App\Http\Controllers
 */
class Index extends Controller
{

    /**
     * @return \Illuminate\View\View
     */
    public function home()
    {
        return view('index.home');
    }

}