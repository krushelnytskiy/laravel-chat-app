<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

/**
 * Class Search
 * @package App\Http\Controllers\Api
 */
class Search extends Controller
{

    /**
     * @param Request $request
     * @return array
     */
    public function search(Request $request)
    {
        $error = ['error' => 'No results found'];

        if (null !== ($keyword = $request->get('keyword'))) {
            $result = User::where('name', 'like', '%' . $keyword .  '%')
                ->take(6)
                ->get(['name', 'id', 'api_token'])
                ->toArray();

            return count($result) > 0 ? $result : $error;
        }

        return $error;
    }

}